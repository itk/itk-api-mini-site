<?xml version="1.0" encoding="UTF-8"?>
<document xmlns="http://maven.apache.org/XDOC/2.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/XDOC/2.0 http://maven.apache.org/xsd/xdoc-2.0.xsd">
 
  <properties>
    <title>ITK API and reference implementation</title>
    <author email="Nicholas.Jones9 at nhs.net">Nick Jones</author>
  </properties>

  <body>
    <div id="container">
    <div id="bodyColumn">
           <div class="section">
                <h2><a name="ADT_Admit_Patient_-_Pipe_and_Hat_format_-_Synchronous"></a>ADT Admit Patient - Pipe and Hat format - Synchronous</h2>
		        <ul class="nav nav-tabs tab-control">
				  <li class="active"><a href="#overview">Overview</a></li>
				  <li><a href="#sender">Sender</a></li>
				  <li><a href="#receiver">Receiver</a></li>
				  <li><a href="#request">Request</a></li>
				  <li><a href="#response">Response</a></li>
				</ul>
           </div>
     </div>

<!-- a href="#" id="example" class="btn btn-success" rel="popover">Hover over me! Please Don't Click</a-->


<div class="row-fluid" style="height:325px" >
    <div class="span5">
      <p class="tab-title">Black Box View</p>
   
       <div class="clickablemapview">
	        <img src="images/ADT - Synchronous - blackbox.jpg" alt="" usemap="#bbviewmap" />
	        <map name="bbviewmap" id="bbviewmap">
	           <area shape="rect" coords="25,35,112,84" alt="" title="Sender" href="#sender"></area>
	           <area shape="rect" coords="284,35,373,84" alt="" title="Receiver" href="#receiver"></area>
	           <area shape="rect" coords="80,118,320,143" alt="" title="Request" href="#request"></area>
	           <area shape="rect" coords="80,160,320,185" alt="" title="Response" href="#response"></area>
	        </map>
        </div>    
    </div>
    <div class="span7">
        <div class="tab-content">
  <div class="tab-pane active" id="overview">
        <p class="tab-title">Overview</p>
        <p>
        The scenario shows a simple synchronous interaction between sender and receiver, as previously described in the SMSP examples.
        This example is drawn out to show how the ITK Reference Implementation manages the transmission of messages in non-xml format. 
        In this case an HL7v2 ADT message in traditional "pipe and hat" format is packaged up
        and sent over the wire with no implications for the sending application and very little
        configuration. Although not shown here, the implications for the receiver are similarly simple.
        </p>
  </div>
  <div class="tab-pane" id="sender">
        <p class="tab-title">Sender View</p>
        <p>
        The Sender builds a <code>SimpleMessage</code> using a String containing the p/h ADT message. Configuration of the service tells
        the R.I. that this messages requires base64 encoding so the RI performs the encoding and builds the DistributionEnvelope with the appropriate
        tagging.
        </p>
  </div>
  <div class="tab-pane" id="receiver">
        <p class="tab-title">Receiver View</p>
        <p>
            The Receiver consumers the SOAP message. This validates the SOAP headers and then extracts the payload. The SOAP payload is then split further
            into Distribution Envelope and business payload. The Distribution Envelope is validated and extracted into <code>MessageProperties</code>.
            The R.I. then looks up the service properties from the configuration and - in this case - as the payload is in base64 form it is decoded before
            being packaged up as a <code>SimpleMessage</code> and passed onto the Application Message Handler.
            </p>
  </div>
  <div class="tab-pane" id="request">
      <p class="tab-title">Request Message
      <a href="samples/A01.xml" target="_blank" type="text/xml">(Get example)</a></p>
		<div class="source">
			<pre class="prettyprint">
&lt;soap:Envelope xmlns:itk="urn:nhs-itk:ns:201005" ... &gt;
 &lt;soap:Header&gt; ... &lt;/soap:Header&gt;
 &lt;soap:Body&gt;
  &lt;itk:DistributionEnvelope ... &gt;
   &lt;itk:header service="urn:nhs-itk:services:201005:admitPatientPH-v1-0" ...&gt;
    ...
   &lt;/itk:header&gt;
   &lt;itk:payloads count="1"&gt;
    &lt;itk:payload id="..."&gt;TVNIfF5+X ... 4MTIzNHwNCg== &lt;/itk:payload&gt;
   &lt;/itk:payloads&gt;
  &lt;/itk:DistributionEnvelope&gt;
 &lt;/soap:Body&gt;
&lt;/soap:Envelope&gt;</pre>
		</div>
  </div>
  <div class="tab-pane" id="response">
      <p class="tab-title">Simple Message Response
      <a href="samples/SMR.xml" target="_blank" type="text/xml">(Get example)</a></p>
		<div class="source">
			<pre class="prettyprint">
&lt;itk:manifest count="1"&gt;
        &lt;itk:manifestitem base64="true"
            id="uuid_6e9306ba-9a09-4479-a719-fcc23234b9ec"
            mimetype="text/plain" profileid="ITKv1.0"/&gt;
    &lt;/itk:manifest&gt;
    &lt;itk:senderAddress uri="urn:nhs-uk:addressing:ods:TESTORGS:ORGA"/&gt;
&lt;/itk:header&gt;
&lt;itk:payloads count="1"&gt;
   &lt;itk:payload id="uuid_6e9306ba-9a09-4479-a719-fcc23234b9ec"&gt;TVNIfF5+XCZ8...
    ....HwNCg==&lt;/itk:payload&gt;
&lt;/itk:payloads&gt;</pre>
		</div>
      
  </div>
</div>
    </div>
</div>
   <div id="bodyColumn3">
          <div class="section">
               <h2>How it all happens....</h2>
	        <ul class="nav nav-tabs tab-control">
			  <li class="active"><a href="#step1">Step 1</a></li>
			  <li><a href="#step2">Step 2</a></li>
			  <li><a href="#step3">Step 3</a></li>
			  <li><a href="#step4">Step 4</a></li>
			</ul>
          </div>
    </div>

       <div class="tab-content">
<div class="row-fluid tab-pane active" id="step1">
	<div class="span5"><p></p>
	    <p><span class="label label-important label-large">Step 1</span> </p>

		<p>The sending application code doesn't need to know that the message in in pipe and hat
		format - it just builds a message, sets the serviceId and sends it, as in the previous synchronous examples.</p>
		<p>The full <a href="xref/uk/nhs/interoperability/client/samples/adt/ADTServlet.html#45">source code for the ADT Servlet can be seen here</a>.</p>
		
	</div>

	<div class="span7">
		<div class="source">
			<pre class="prettyprint">
// Create the message
ITKMessage msg = new SimpleMessage();
msg.setBusinessPayload(phMessage);
...
mp.setServiceId("urn:nhs-itk:services:201005:admitPatientPH-v1-0");
...
// Send this message synchronously.
ITKMessage responseMsg = sender.sendSync(msg);</pre>
		</div>

	</div>

</div>
<div class="row-fluid tab-pane" id="step2">
	<div class="span5"><p></p>
	    <p><span class="label label-important label-large">Step 2</span> </p>
		<p>
		A raw HL7v2 message in p/h format would not be valid data if just wrapped up in
		XML, so the ITK R.I. needs to know that this message is p/h so that it can construct the on-the-wire message accordingly.
		The R.I. uses a <code>service.properties</code> file to contain service configurations such as this.
		</p>
	</div>
	<div class="span7">

		<div class="source">
			<pre class="prettyprint">

urn\:nhs-itk\:services\:201005\:admitPatientPH-v1-0.isBase64=Y
urn\:nhs-itk\:services\:201005\:admitPatientPH-v1-0.mimeType=text/plain
			</pre>
		</div>

	</div>


</div>
<div class="row-fluid tab-pane" id="step3">
	<div class="span5"><p></p>
	    <p><span class="label label-important label-large">Step 3</span> </p>
		<p>
		The Reference Implementation uses the service configuration to 
		encode the message using core java language conversions.		</p>
	</div>
	<div class="span7">

		<div class="source">
			<pre class="prettyprint">
if(service.isBase64()){
	String b64DocText = javax.xml.bind.DatatypeConverter.
  		printBase64Binary(request.getBusinessPayload().getBytes());
	request.setBusinessPayload(b64DocText);
}
message = new DEWrappedMessage(service, request, true);
			</pre>
		</div>

	</div>

</div>

<div class="row-fluid tab-pane" id="step4">
	<div class="span5"><p></p>
	    <p><span class="label label-important label-large">Step 4</span> </p>
		<p>
		Now the R.I. sets the mime type, base64 encoding to the appropriate values
		and embeds the encoded message as the payload.
		</p>
	</div>
	<div class="span7">

		<div class="source">
			<pre class="prettyprint">
	&lt;itk:manifest count="1">
		&lt;itk:manifestitem base64="true"
			id="uuid_6e9306ba-9a09-4479-a719-fcc23234b9ec"
			mimetype="text/plain" profileid="ITKv1.0"/>
	&lt;/itk:manifest>
	&lt;itk:senderAddress uri="urn:nhs-uk:addressing:ods:TESTORGS:ORGA"/>
&lt;/itk:header>
&lt;itk:payloads count="1">
   &lt;itk:payload id="uuid_6e9306ba-9a09-4479-a719-fcc23234b9ec">TVNIfF5+XCZ8...
	....HwNCg==&lt;/itk:payload>
&lt;/itk:payloads>
			</pre>
		</div>

	</div>


</div>

</div>

</div>
</body>
</document>