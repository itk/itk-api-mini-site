<xsl:stylesheet version="2.0" 
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns="http://maven.apache.org/XDOC/2.0"
        xmlns:pom="http://maven.apache.org/POM/4.0.0"
        xmlns:xtr="http://www.xml-solutions.com/xuse/trace-report"
        exclude-result-prefixes="xs xtr pom">
        
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:variable name="report-path" select="'/target/generated-sources/annotations/trace-report.xml'"/>    
    <xsl:variable name="aggregate">        
        <xtr:trace-report>
	        <xsl:for-each select="/pom:project/pom:modules/pom:module">
	            
	            <xsl:variable name="rel-path" select="concat('../../',., $report-path)"/>
	            <xsl:message>[INFO] Attempting to load trace report from: <xsl:value-of select="$rel-path"/></xsl:message>
	            <xsl:choose>
	                <xsl:when test="doc-available($rel-path)">
	                    <xsl:variable name="current-report" select="doc($rel-path)"/>
	                    <xsl:apply-templates select="$current-report" mode="aggregate"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:message>[INFO] No trace-report found at: <xsl:value-of select="$rel-path"/></xsl:message>
                    </xsl:otherwise>
	            </xsl:choose>
	            
	        </xsl:for-each>
        </xtr:trace-report>
    </xsl:variable>
    
    <xsl:variable name="req-prefix-map">
        <map xmlns="">
            <entry prefix="WS">ITK Web Service requirements</entry>
            <entry prefix="IFA">Infrastructure requirements</entry>
            <entry prefix="COR">Core requirements</entry>
        </map>
    </xsl:variable>
    
    <xsl:variable name="all-artefacts" select="$aggregate/xtr:trace-report/xtr:artefact"/>
    
    <xsl:template match="xtr:trace-report/xtr:artefact" mode="aggregate">
        <xsl:sequence select="."/>
    </xsl:template>
    
    
    <xsl:template match="/">
		<document>			
		  <properties>
		    <title>ITK requirements and Reference Implementation Cross-reference</title>
		    <author>Auto generated</author>
		  </properties>
		  <body>
			  <section name="Introduction">
			     <p>This page is an experimental feature that shows traceability between the ITK transport and distribution requirements and the codebase (including the API, reference implementation and samples project).</p>
			     <p>The intention is both to show the requirments coverage of the reference implementation as well as allowing readers to jump to the specific parts of the API and reference implementation that trace to a particular requirement they are interested in.</p>
			     <subsection name="What is the status of the requirement traceability?">
			         <p>Currently there are some requirement annotations in the API and reference implementation codebase to illustrate how the traceability might function. The traceability is not complete - however we are looking for feedback (positive and negative) to understand how useful (or not) such tracebility is.</p>
			     </subsection>
			     <subsection name="Why can't I see the requirement text?">
                     <p>At the moment the trace table (below) is entirely built from the annotation references in the source code. Our ambition is to eventually show the requirement text in the table below however we would need a machine processable list of requirements to achieve this.</p>
                 </subsection>
			  </section>
			  <section name="Requirements Traceability">
			      
			      <!-- xsl:message>Aggregate: <xsl:sequence select="$aggregate"/></xsl:message-->
                  <xsl:apply-templates select="$aggregate/xtr:trace-report"/>
                  
                  <subsection name="Key">
                     <p>The following table is a guide to the implementation status of the traced requirements</p>
                     <table>
                         <thead>
                             <tr>
                                 <th width="20%">Item</th>
                                 <th width="80%">Description</th>
                             </tr>
                         </thead>
                         <tbody>
                             <tr>
                                 <td>-</td>
                                 <td>If the requirement you are interested in is not represented in the trace table (above) then it has not been traced to the API and reference implementation codebase - the implementation status is effectively "unknown"</td>
                             </tr>
                             
                             <tr>
                                 <td class="not-started">REQ-##</td>
                                 <td>The requirement has been traced to the codebase, however implementation of the necessary logic has not been started - i.e. there are only code stubs</td>
                             </tr>
                             <tr>
                                 <td class="in-progress">REQ-##</td>
                                 <td>Traced requirements in the codebase indicate that the of the functionality is in-place but the requirement has not been fully realised</td>
                             </tr>
                             
                             <tr>
                                 <td class="implemented">REQ-##</td>
                                 <td>All traced references of the requirement in the codebase indicate that the requirement has been fully-implemented</td>
                             </tr>
                         </tbody>
                     </table>
                  </subsection>
               </section>
		  </body>
        </document>
    </xsl:template>
    
    
    
    <xsl:template match="xtr:trace-report">
        <xsl:for-each-group select=".//xtr:trace-to" group-by="substring(.,1,3)">
            <!-- Strip off any trailing "-" if present -->
            <xsl:variable name="prefix" select="if (ends-with(current-grouping-key(), '-')) then  substring(current-grouping-key(),1,2) else current-grouping-key()"/>
	        <xsl:call-template name="do-req-group">
	            <xsl:with-param name="group-prefix" select="$prefix"/>
	        </xsl:call-template>
        </xsl:for-each-group>
    </xsl:template>
    
    <xsl:template name="do-req-group">
        <xsl:param name="group-prefix"/>
        <subsection name="{$group-prefix} - {xtr:get-group-title($group-prefix)}">
	        <table>
	            <thead>
	                <tr>
	                    <th width="20%">Requirement</th>
	                    <th width="80%">Reference Implementation Code</th>
	                </tr>
	            </thead>
	            <xsl:for-each-group select="//xtr:trace-to[@class='requirement'][starts-with(., $group-prefix)]" group-by=".">
	                <tr>
	                   <td class="{xtr:get-overall-status(current-group())}"><xsl:value-of select="current-grouping-key()"/></td>
	                   <td>
	                       <xsl:variable name="artefacts" select="$all-artefacts[xtr:trace-to/text() = current-grouping-key()]"/>
	                       <xsl:for-each select="$artefacts">
	                           <li><a href="xref/{@uri}"><xsl:value-of select="xtr:id"/></a> (<xsl:value-of select="@type"/>)</li>
	                       </xsl:for-each>
	                   </td>
	                </tr>
	            </xsl:for-each-group>
	        </table>
        </subsection>
    </xsl:template>
    
    <xsl:function name="xtr:get-group-title" as="xs:string">
        <xsl:param name="prefix" as="xs:string?"/>
        <!-- xsl:message>Looking for title for prefix: <xsl:sequence select="$prefix"/> in map <xsl:sequence select="$req-prefix-map"/></xsl:message-->
        <xsl:sequence select="$req-prefix-map//entry[@prefix = $prefix]/text()"/>
    </xsl:function>
    
    <!-- Assume requirement implementation statuses of implemented, in-progress, not-started, abstract -->
    <xsl:function name="xtr:get-overall-status" as="xs:string">
        <xsl:param name="reqs"/> 
        <!-- Logic is:
            If all traced requirements have a status of "not started" then aggregate status is "not-started"
            If all traced requirements have a status of "abstract" then aggregate status is "abstract"
            If there is at least one "implemented" requirement and no counts of "in-progress" or "not-started" then aggregate status is "implemented"
            If any of the traced requirements have a status of "in-progress" or there is a mixture of "not-started" and "implemented" then aggregate status is "in-progress"           
         -->
        <xsl:variable name="req-ref-count" select="count($reqs)"/>
        <xsl:sequence select="if (count($reqs[@status = 'not-started']) = $req-ref-count) then 'not-started'
                                else if (count($reqs[@status = 'abstract']) = $req-ref-count) then 'abstract'
                                else if (count($reqs[@status = 'implemented']) &gt; 0 and count($reqs[@status = 'not-started']) = 0 and count($reqs[@status = 'in-progress']) = 0) then 'implemented'
                                else if (count($reqs[@status = 'in-progress']) &gt; 0 or (count($reqs[@status = 'implemented']) &gt; 0 and count($reqs[@status = 'not-started']) &gt; 0)) then 'in-progress'
                                else 'unknown'"/>
    </xsl:function>
    
</xsl:stylesheet>    